# 🚀 L2_CDA_ModuleImage 📸

Bienvenue sur le dépôt du projet `L2_CDA_ModuleImage`! Ce projet est une réalisation de Luka Coutant (P2202851), Eymeric Dechelette (P2205912), et Tom Bouillot (P2202588). 🎓👨‍💻

## 🎯 Qu'est-ce que c'est?

`L2_CDA_ModuleImage` est un projet de gestion d'images. Il permet d'afficher une image, de dessiner des rectangles, et bien plus encore! 🎨🖼️

## 🚀 Comment compiler?

Pour compiler il faut ce rendre dans le dossier build. (`cd build`)
Lancer : `cmake ..` puis `make`
Vous pouvez aprés lancer les binaire dans le dossier bin : 
- affichage
- exemple
- test

## 🎉 Merci!

Merci de visiter notre projet! Nous espérons que vous le trouverez intéressant et utile! 🙏