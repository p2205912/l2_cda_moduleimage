/**
 * @file Image.cpp
 * @brief Implementation file for the Image class.
 */
#include "Image.h"

#include <fstream>
#include <cassert>

Image::Image()
{
    dimx = 0;
    dimy = 0;
    tab = nullptr;
}

Image::Image(int dimx, int dimy)
{
    assert(dimx >= 0 && dimy >= 0);
    this->dimx = dimx;
    this->dimy = dimy;
    tab = new Pixel[dimx * dimy];
}

Image::~Image()
{
    if (tab != nullptr)
    {
        delete[] tab;
    }
    dimx = 0;
    dimy = 0;
}

void Image::dessinerRectangle(int Xmin, int Ymin, int Xmax, int Ymax, const Pixel &couleur)
{
    assert(Xmin >= 0 && Xmin < dimx && Ymin >= 0 && Ymin < dimy);
    assert(Xmax >= 0 && Xmax < dimx && Ymax >= 0 && Ymax < dimy);
    assert(Xmin <= Xmax && Ymin <= Ymax);
    for (int i = Xmin; i <= Xmax; i++)
    {
        for (int j = Ymin; j <= Ymax; j++)
        {
            setPixel(i, j, couleur);
        }
    }
}

Pixel &Image::getPixel(int x, int y) const
{
    assert(x >= 0 && x < dimx && y >= 0 && y < dimy);
    return tab[y * dimx + x];
}

void Image::setPixel(int x, int y, const Pixel &couleur)
{
    assert(x >= 0 && x < dimx && y >= 0 && y < dimy);
    tab[y * dimx + x] = couleur;
}

int Image::getDimx() const
{
    return dimx;
}

int Image::getDimy() const
{
    return dimy;
}

Pixel *Image::getTab() const
{
    return tab;
}

void Image::effacer(const Pixel &couleur)
{
    dessinerRectangle(0, 0, dimx - 1, dimy - 1, couleur);
}

void Image::sauver(const string &filename) const
{
    ofstream fichier(filename.c_str());
    assert(fichier.is_open());
    fichier << "P3" << endl;
    fichier << dimx << " " << dimy << endl;
    fichier << "255" << endl;
    for (unsigned int y = 0; y < dimy; ++y)
        for (unsigned int x = 0; x < dimx; ++x)
        {
            Pixel pix = getPixel(x, y);
            fichier << +pix.r << " " << +pix.g << " " << +pix.b << " ";
        }
    cout << "Sauvegarde de l'image " << filename << " ... OK\n";
    fichier.close();
}

void Image::ouvrir(const string &filename)
{
    ifstream fichier(filename.c_str());
    assert(fichier.is_open());
    char r, g, b;
    string mot;
    dimx = dimy = 0;
    fichier >> mot >> dimx >> dimy >> mot;
    assert(dimx > 0 && dimy > 0);
    if (tab != nullptr)
        delete[] tab;
    tab = new Pixel[dimx * dimy];
    for (unsigned int y = 0; y < dimy; ++y)
        for (unsigned int x = 0; x < dimx; ++x)
        {
            fichier >> r >> b >> g;
            setPixel(x, y, Pixel(r, g, b));
        }
    fichier.close();
    cout << "Lecture de l'image " << filename << " ... OK\n";
}

void Image::afficherConsole() const
{
    cout << dimx << " " << dimy << endl;
    for (unsigned int y = 0; y < dimy; ++y)
    {
        for (unsigned int x = 0; x < dimx; ++x)
        {
            Pixel &pix = getPixel(x, y);
            cout << +pix.r << " " << +pix.g << " " << +pix.b << " ";
        }
        cout << endl;
    }
}

void Image::testRegression()
{
    /*On va tester : Image() et Image(int dimx, int dimy) en même temps
     */
    Image img1;
    Image img2(10, 10);
    assert(img1.dimx == 0 && img1.dimy == 0 && img1.tab == nullptr);
    assert(img2.dimx == 10 && img2.dimy == 10 && img2.tab != nullptr);
    /*On va tester : getPixel(int x, int y) et getPixel(int x, int y) const
     */
    Pixel pixel1 = img2.getPixel(0, 0);
    Pixel pixel2 = img2.getPixel(9, 9);
    assert(pixel1.r == 0 && pixel1.g == 0 && pixel1.b == 0);
    assert(pixel2.r == 0 && pixel2.g == 0 && pixel2.b == 0);
    /*On va tester : setPixel(int x, int y, Pixel couleur)
     */
    Pixel pixel3(255, 255, 255);
    img2.setPixel(0, 0, pixel3);
    assert(img2.getPixel(0, 0).r == 255 && img2.getPixel(0, 0).g == 255 && img2.getPixel(0, 0).b == 255);
    /*On va tester : dessinerRectangle(int Xmin, int Ymin, int Xmax, int Ymax, Pixel couleur)
     */
    Pixel pixel4(10, 10, 10);
    img2.dessinerRectangle(0, 0, 6, 6, pixel4);
    for (int i = 0; i < img2.dimx; i++)
    {
        for (int j = 0; j < img2.dimy; j++)
        {
            if (i <= 6 && j <= 6)
            {
                assert(img2.getPixel(i, j).r == 10 && img2.getPixel(i, j).g == 10 && img2.getPixel(i, j).b == 10);
            }
            else
            {
                assert(img2.getPixel(i, j).r == 0 && img2.getPixel(i, j).g == 0 && img2.getPixel(i, j).b == 0);
            }
        }
    }
    /*On va tester : effacer(Pixel couleur)
     */
    Pixel pixel5(255, 255, 255);
    img2.effacer(pixel5);
    assert(img2.getPixel(0, 0).r == 255 && img2.getPixel(0, 0).g == 255 && img2.getPixel(0, 0).b == 255);
    assert(img2.getPixel(9, 9).r == 255 && img2.getPixel(9, 9).g == 255 && img2.getPixel(9, 9).b == 255);
}
