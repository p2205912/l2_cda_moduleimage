/**
 * @file Image.h
 * @brief Header file for the Image class.
 */

#ifndef _IMAGE_H
#define _IMAGE_H

#include "Pixel.h"

using namespace std;

/**
 * @class Image
 * @brief Represents an image composed of pixels.
 */
class Image
{
private:
    int dimx;   ///< The width of the image.
    int dimy;   ///< The height of the image.
    Pixel *tab; ///< Pointer to the pixel array.

public:
    /**
     * @brief Default constructor.
     * Initializes dimx and dimy to 0 and does not allocate memory for the pixel array.
     */
    Image();

    /**
     * @brief Constructor.
     * Initializes dimx and dimy (after verification) and allocates memory for the pixel array (black image).
     * @param dimx The width of the image.
     * @param dimy The height of the image.
     */
    Image(int dimx, int dimy);

    /**
     * @brief Destructor.
     * Deallocates the memory of the pixel array and updates dimx and dimy to 0.
     */
    ~Image();

    /**
     * @brief Retrieves the original pixel at the specified coordinates (x, y) by checking its validity.
     * The formula to convert from a 2D array to a 1D array is tab[y * dimx + x].
     * @param x The x-coordinate of the pixel.
     * @param y The y-coordinate of the pixel.
     * @return The original pixel at the specified coordinates.
     */
    Pixel &getPixel(int x, int y) const;

    /**
     * @brief Retrieves the width of the image.
     * @return The width of the image.
     */
    int getDimx() const;

    /**
     * @brief Retrieves the height of the image.
     * @return The height of the image.
     */
    int getDimy() const;

    /**
     * @brief Retrieves the pixel array.
     * @return The pixel array.
     */
    Pixel *getTab() const;

    /**
     * @brief Modifies the pixel at the specified coordinates (x, y).
     * @param x The x-coordinate of the pixel.
     * @param y The y-coordinate of the pixel.
     * @param couleur The new color of the pixel.
     */
    void setPixel(int x, int y, const Pixel &couleur);

    /**
     * @brief Draws a filled rectangle of the specified color in the image.
     * @param Xmin The minimum x-coordinate of the rectangle.
     * @param Ymin The minimum y-coordinate of the rectangle.
     * @param Xmax The maximum x-coordinate of the rectangle.
     * @param Ymax The maximum y-coordinate of the rectangle.
     * @param couleur The color of the rectangle.
     */
    void dessinerRectangle(int Xmin, int Ymin, int Xmax, int Ymax, const Pixel &couleur);

    /**
     * @brief Clears the image by filling it with the specified color.
     * @param couleur The color to fill the image with.
     */
    void effacer(const Pixel &couleur);

    /**
     * @brief Performs a series of tests to verify that all functions work correctly
     * and that the object's data members are consistent.
     */
    static void testRegression();

    /**
     * @brief Saves the image to a file in PPM format.
     * @param filename The name of the file to save the image to.
     */
    void sauver(const std::string &filename) const;

    /**
     * @brief Opens an image from a file in PPM format.
     * @param filename The name of the file to open the image from.
     */
    void ouvrir(const std::string &filename);

    /**
     * @brief Displays the image in the console.
     */
    void afficherConsole() const;
};

#endif