/**
 * @file ImageViewer.cpp
 * @brief Implementation file for the ImageViewer class.
 */

#include "ImageViewer.h"

ImageViewer::ImageViewer()
{
    SDL_Init(SDL_INIT_VIDEO);
    window = SDL_CreateWindow("ImageViewer", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, WINDOW_WIDTH, WINDOW_HEIGHT, SDL_WINDOW_SHOWN);
    renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
}

ImageViewer::~ImageViewer()
{
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_DestroyTexture(texture);
    SDL_Quit();
}
void ImageViewer::afficher(const Image &im)
{

    // create a surface from the image data
    surface = SDL_CreateRGBSurfaceFrom(im.getTab(), im.getDimx(), im.getDimy(), 24, im.getDimx() * 3, 0, 0, 0, 0);
    texture = SDL_CreateTextureFromSurface(renderer, surface);
    SDL_FreeSurface(surface);

    // define the source area (entire image)
    srcRect.x = 0;
    srcRect.y = 0;
    srcRect.w = im.getDimx();
    srcRect.h = im.getDimy();

    // define the destination area (centered and scaled)
    dstRect.x = (WINDOW_WIDTH - im.getDimx() * zoom) / 2;
    dstRect.y = (WINDOW_HEIGHT - im.getDimy() * zoom) / 2;
    dstRect.w = im.getDimx() * zoom;
    dstRect.h = im.getDimy() * zoom;

    // Boucle d'événements
    bool running = true;
    while (running)
    {
        SDL_Event event;
        while (SDL_PollEvent(&event))
        {
            switch (event.type)
            {
            case SDL_QUIT:
                running = false;
                break;
            case SDL_KEYDOWN:
                switch (event.key.keysym.sym)
                {
                case KEY_ESCAPE:
                    running = false;
                    break;
                case KEY_T:
                    zoom *= ZOOM_FACTOR; // Zoom avant
                    dstRect.w = im.getDimx() * zoom;
                    dstRect.h = im.getDimy() * zoom;
                    dstRect.x = (WINDOW_WIDTH - im.getDimx() * zoom) / 2;
                    dstRect.y = (WINDOW_HEIGHT - im.getDimy() * zoom) / 2;
                    break;
                case KEY_G:
                    zoom /= ZOOM_FACTOR; // Zoom arrière
                    dstRect.w = im.getDimx() * zoom;
                    dstRect.h = im.getDimy() * zoom;
                    dstRect.x = (WINDOW_WIDTH - im.getDimx() * zoom) / 2;
                    dstRect.y = (WINDOW_HEIGHT - im.getDimy() * zoom) / 2;
                    break;
                }
                break;
            }

            // Remplir la fenêtre avec du gris clair
            SDL_SetRenderDrawColor(renderer, 220, 220, 220, 255);
            SDL_RenderClear(renderer);

            // Afficher l'image
            SDL_RenderCopy(renderer, texture, &srcRect, &dstRect);

            // Mettre à jour l'écran
            SDL_RenderPresent(renderer);
        }
    }
}
