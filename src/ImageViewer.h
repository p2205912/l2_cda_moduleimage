/**
 * @file ImageViewer.h
 * @brief Header file for ImageViewer class.
 */

#ifndef IMAGEVIEWER_H
#define IMAGEVIEWER_H

#include <SDL2/SDL.h>
#include "Image.h"

/**
 * @brief Window width.
 */
#define WINDOW_WIDTH 500
/**
 * @brief Window height.
 */
#define WINDOW_HEIGHT 500
/**
 * @brief Zoom factor.
 */
#define ZOOM_FACTOR 1.1f

/**
 * @brief Enumeration for keyboard keys.
 */
enum Key
{
    /**
     * @brief Escape key.
     */
    KEY_ESCAPE = 27,
    /**
     * @brief T key.
     */
    KEY_T = 116,
    /**
     * @brief G key.
     */
    KEY_G = 103
};

/**
 * @class ImageViewer
 * @brief Class for image viewing.
 */
class ImageViewer
{
private:
    SDL_Window *window;     ///< SDL_Window pointer
    SDL_Renderer *renderer; ///< SDL_Renderer pointer
    double zoom = 1.0f;     ///< Zoom factor
    SDL_Surface *surface;   ///< SDL_Surface pointer
    SDL_Texture *texture;   ///< SDL_Texture pointer
    SDL_Rect srcRect;       ///< SDL_Rect for source area
    SDL_Rect dstRect;       ///< SDL_Rect for destination area
    SDL_Point imageCenter;  ///< SDL_Point for image center

public:
    /**
     * @brief Constructor that initializes all SDL2 and creates the window and the renderer.
     */
    ImageViewer();

    /**
     * @brief Destructor that destroys and closes SDL2.
     */
    ~ImageViewer();

    /**
     * @brief Displays the image passed as a parameter and allows (de)zoom.
     * @param im Image data to display.
     */
    void afficher(const Image &im);
};

#endif // IMAGEVIEWER_H