/**
 * @file Pixel.cpp
 * @brief Implementation file for the Pixel class.
 */
#include "Pixel.h"

Pixel::Pixel()
{
    r = 0;
    g = 0;
    b = 0;
}

Pixel::Pixel(unsigned char r, unsigned char g, unsigned char b)
{
    this->r = r;
    this->g = g;
    this->b = b;
}

void Pixel::afficherPixel() const
{
    std::cout << "Rouge : " << (int)r << std::endl;
    std::cout << "Vert : " << (int)g << std::endl;
    std::cout << "Bleu : " << (int)b << std::endl;
}
