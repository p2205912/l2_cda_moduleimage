/**
 * @file Pixel.h
 * @brief Header file for the Pixel.
 */

#ifndef _PIXEL_H
#define _PIXEL_H

#include <iostream>

/**
 * @brief The Pixel struct represents a pixel in an image.
 */
struct Pixel
{
    /**
     * @brief Default constructor for the Pixel struct.
     */
    Pixel();

    /**
     * @brief Constructor for the Pixel struct.
     * @param r The red component of the pixel.
     * @param g The green component of the pixel.
     * @param b The blue component of the pixel.
     */
    Pixel(unsigned char r, unsigned char g, unsigned char b);

    /**
     * @brief Prints the pixel values to the console.
     */
    void afficherPixel() const;

    unsigned char r; /**< The red component of the pixel. */
    unsigned char g; /**< The green component of the pixel. */
    unsigned char b; /**< The blue component of the pixel. */
};

#endif